PUT /absence
{
	"settings" : {
		"index" : {
			"number_of_shards" : 1,
			"number_of_replicas" : 0,
			"number_of_routing_shards" : 2,
			"analysis": {
				"analyzer" : {
					"default": {
						"type": "french"
					}
				}
			}
		}	
	},
	"mappings":{
		"properties":{
			"id":{
				"type":"keyword"
			},
			"idMedecin":{
				"type":"keyword"
			},
			"dateDebutAbsence":{
				"type":"date",
				"format":"dd/MM/YY"
			},
			"dateFinAbsence":{
				"type":"date",
				"format":"dd/MM/YY"
			},
			"dateInsertion":{
				"type":"date",
				"format":"dd/MM/YY"
			}
		}
	}
}