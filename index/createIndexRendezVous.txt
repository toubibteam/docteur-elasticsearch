PUT /rendez_vous
{
	"settings" : {
		"index" : {
			"number_of_shards" : 1,
			"number_of_replicas" : 0,
			"number_of_routing_shards" : 2,
			"analysis": {
				"analyzer" : {
					"default": {
						"type": "french"
					}
				}
			}
		}	
	},
	"mappings":{
		"properties":{
			"id":{
				"type":"keyword"
			},
			"idMedecin":{
				"type":"keyword"
			},
			"idPatient":{
				"type":"keyword"
			},
			"idCabinet":{
				"type":"keyword"
			},
			"idMotif":{
				"type":"keyword"
			},
			"descriptionMotif":{
				"type":"keyword"
			},
			"consultationDomicile":{
				"type":"boolean"
			},
			"dateDebutRdv":{
				"type":"date",
				"format":"dd/MM/YY"
			},
			"dateFinRdv":{
				"type":"date",
				"format":"dd/MM/YY"
			},
			"statut":{
				"type":"keyword"
			},
			"dateInsertion":{
				"type":"date",
				"format":"dd/MM/YY"
			}
		}
	}
}